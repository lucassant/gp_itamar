<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DepartamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $deptos = Departamento::orderBy('id')->paginate(15);

        return view('departamentos', compact('deptos'));
    }

    public function importarDepartamentos()
    {
        $response = Http::get('http://131.100.251.82:8086/construshow_api/public/api/deptos');
        $deptos = json_decode($response);

        foreach ($deptos as $p) {

            Departamento::updateOrCreate(
                ['id' => $p->iddepto],
                [
                    'nome' => $p->descricao,
                ]
            );
        }

        $deptos = Departamento::orderBy('id')->paginate(15);

        return redirect()->back()->with('alert', 'Importação finalizada com sucesso!')->with(compact($deptos));
    }

    public function edit(Request $request)
    {
        $depto = Departamento::find($request->id);

        return view('departamento-form', compact('depto'));
    }

    public function update(Request $request, $id)
    {
        $depto = Departamento::find($id);

        $depto->desconto_maximo = Str::replaceArray('.', [''], $request->desconto_maximo);
        $depto->desconto_maximo = Str::replaceArray(',', ['.'], $depto->desconto_maximo);

        $depto->acrescimo_maximo = Str::replaceArray('.', [''], $request->acrescimo_maximo);
        $depto->acrescimo_maximo = Str::replaceArray(',', ['.'], $depto->acrescimo_maximo);

        $depto->save();

        return redirect()->back()->with('alert', 'Departamento atualizado com sucesso!');
    }

    public function searchDepto(Request $request)
    {
        $search = $request->get('q');
        $deptos = Departamento::where('nome', 'LIKE', $search . '%')->orWhere('id', $search)->paginate(20);
        return view('departamentos', compact('deptos'));
    }

    public function getAll()
    {
        return Departamento::find(1);
    }
}
