<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use App\Produto;
use App\SaldoFLex;

class ApiController extends Controller
{
    public function departamentos()
    {
        $retorno = Departamento::all();
        return json_encode((object) array('departamentos' => $retorno));
    }

    public function produtos(String $dataSinc)
    {
        $retorno = Produto::select('id', 'preco_imposto', 'peso_unidade')->where('updated_at', '>=', $dataSinc)->get();
        return json_encode((object) array('produtos' => $retorno));
    }

    public function getFlex($id)
    {
        return SaldoFLex::where('id_vendedor', $id)->orderBy('id', 'desc')->limit(1)->get();
    }

    public function getAllFlex($id)
    {
        return SaldoFLex::where('id_vendedor', $id)->orderBy('id', 'desc')->get();
    }
}
