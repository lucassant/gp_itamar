@extends('layouts.app')

@section('content')
<div class="container">
    <form class="form-horizontal" action="/update-depto/{{ $depto->id }}" method="post">
        <fieldset>
            <legend>Alterar produto</legend>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label for="inputEmail" class="col-md-4 ">Código</label>
                <div class="col-md-2">
                    <input class="form-control" name="codigo" id="codigo" type="number" value="{{ $depto->id }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-10">
                    <input class="form-control" name="nome" type="text" id="nome" value="{{ $depto->nome }}" disabled>
                </div>
            </div>
            <div class="row col-md-12">
                <div class="form-group">
                    <div class="col-md-12">

                        <label for="desconto_maximo">Desconto máximo</label>
                        <div>
                            <input class="form-control desconto_maximo" name="desconto_maximo" id="desconto_maximo" type="text" value="{{ $depto->desconto_maximo }}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">

                        <label for="acrescimo_maximo">Acréscimo máximo</label>
                        <div>
                            <input class="form-control acrescimo_maximo" name="acrescimo_maximo" type="text" id="acrescimo_maximo" value="{{ $depto->acrescimo_maximo }}" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Salvar</button>

                </div>
            </div>
        </fieldset>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    $(document).ready(function($) {
        $('.desconto_maximo').mask('000.000.000,00', {
            reverse: true
        });
        $('.acrescimo_maximo').mask('000.000.000,00', {
            reverse: true
        });

    })
</script>
<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection