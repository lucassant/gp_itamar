@extends('layouts.app')

@section('content')
<div class="container">
    <form class="form-horizontal" action="/update-produto/{{ $produto->id }}" method="post">
        <fieldset>
            <legend>Alterar produto</legend>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label for="inputEmail" class="col-md-4 ">Código</label>
                <div class="col-md-2">
                    <input class="form-control" name="codigo" id="codigo" type="number" value="{{ $produto->id }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-10">
                    <input class="form-control" name="nome" type="text" id="nome" value="{{ $produto->nome }}" disabled>
                </div>
            </div>
            <div class="row col-md-12">
                <div class="form-group">
                    <div class="col-md-12">

                        <label for="preco_imposto">Imposto (%)</label>
                        <div>
                            <input class="form-control preco_imposto" name="preco_imposto" id="preco_imposto" type="text" value="{{ $produto->preco_imposto }}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">

                        <label for="peso_unidade">Peso da unidade (Kg)</label>
                        <div>
                            <input class="form-control peso_unidade" name="peso_unidade" type="text" id="peso_unidade" value="{{ $produto->peso_unidade }}" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Salvar</button>

                </div>
            </div>
        </fieldset>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    $(document).ready(function($) {
        $('.preco_imposto').mask('000.000.000,00', {
            reverse: true
        });
        $('.peso_unidade').mask('000.000.000,00', {
            reverse: true
        });
    })
</script>
<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection