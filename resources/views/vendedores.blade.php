@extends('layouts.app')

@section('content')
<div class="container">

    <a href="btn-importar-rep" type="button" class="btn btn-dark">Importar representantes</a>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <br />
            <div class="card">
                <div class="card-header">Representantes</div>

                <div class="card-body">

                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($reps as $r)
                            <tr>
                                <th scope="row">{{ $r->id }}</th>
                                <td>{{ $r->nome }}</td>
                                <td>{{ $r->email }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="mx-auto">
                    {{ $reps->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection